#pragma once
#include <memory>

struct Breakes
{
    virtual int stop(int speed) = 0;
    virtual ~Breakes(){}
};

using BreakesPtr = std::unique_ptr<Breakes>;

class SuperBreakes : public Breakes
{
    int stop(int) override
    {
        return 0;
    }
};

class LogarithmicBreakes : public Breakes
{
    int stop(int speed) override
    {
        if (speed < 50)
        {
            return 0;
        }
        return speed/2;
    }
};
