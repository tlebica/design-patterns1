#include "SpeedLimit.hpp"
#include <cmath>

int SpeedLimit::limitSpeed(int speed)
{
    return speed;
}

int ConstSpeedLimit::limitSpeed(int speed)
{
    return std::min(speed,limit);
}

int CxSpeedLimit::limitSpeed(int speed)
{
    return std::min(static_cast<double>(speed), HP/cx);
}
