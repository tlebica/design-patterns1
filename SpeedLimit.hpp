#pragma once
#include <memory>

struct SpeedLimit
{
public:
    virtual int limitSpeed(int speed);
    virtual ~SpeedLimit(){}
};

using SpeedLimitPtr = std::unique_ptr<SpeedLimit>;

struct ConstSpeedLimit: SpeedLimit
{
public:
    ConstSpeedLimit(int limit)
        :limit(limit)
    {

    }
    int limitSpeed(int speed) override;
private:
    int limit;
};

class CxSpeedLimit: public SpeedLimit
{
public:
    CxSpeedLimit(double cx, int HP)
        :cx(cx),
         HP(HP)
    {}
    int limitSpeed(int speed) override;
private:
    double cx;
    int HP;
};
