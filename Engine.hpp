#pragma once
#include <memory>
#include <cmath>

struct Engine
{
    virtual int accelerate() = 0;
    virtual void reset(){}
    virtual ~Engine(){}
};

using EnginePtr = std::unique_ptr<Engine>;

class LinearEngine : public Engine
{
public:
    LinearEngine(int HP)
        :HP(HP)
    {

    }

    int accelerate()
    {
        return HP;
    }

private:
    int HP;
};

class LogarithmicEngine : public Engine
{
public:
    LogarithmicEngine(int HP)
        :HP(HP)
    {

    }

    int accelerate() override
    {
        return HP/std::pow(2, counter++);
    }

    void reset() override
    {
        counter = 0;
    }

private:
    int HP;
    int counter = 0;
};
