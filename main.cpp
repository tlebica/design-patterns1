#include <iostream>
#include <string>
#include <memory>
#include <vector>

#include "CarFactory.hpp"
#include "TestTrack.hpp"

using Cars = std::vector<CarPtr>;

void testCarsOnTheTestTrack(TestTrack& testTrack, Cars& cars)
{
    int index = 0;
    for(auto& car : cars)
    {
        std::cout << "Car " << ++index << (testTrack.testCar(*car)?" succeded": " failed") << " on the test track" << std::endl;
    }
}

int main()
{
    TestTrack testTrack{};
    Cars cars;
    cars.emplace_back(createPorsche());
    cars.emplace_back(createFerrari());
    cars.emplace_back(createLamborghini());
    cars.emplace_back(createMcLaren());

    testCarsOnTheTestTrack(testTrack, cars);
}
