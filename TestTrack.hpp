#pragma once

class Car;

class TestTrack
{
public:
    bool testCar(Car&);
    bool testAcceleration(Car&);
    bool testBreakes(Car&);
    bool testEnter(Car&);
    bool testLeave(Car&);
    virtual ~TestTrack(){}
};
