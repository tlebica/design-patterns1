#include "CarFactory.hpp"

CarPtr createPorsche()
{
    return CarPtr(new Car(EnginePtr(new LinearEngine(200)), BreakesPtr(new LogarithmicBreakes()), SpeedLimitPtr(new CxSpeedLimit(0.66, 200))));
}

CarPtr createFerrari()
{
    return CarPtr(new Car(EnginePtr(new LogarithmicEngine(300)), BreakesPtr(new SuperBreakes()), SpeedLimitPtr(new SpeedLimit())));
}

CarPtr createLamborghini()
{
    return CarPtr(new Car(EnginePtr(new LinearEngine(400)), BreakesPtr(new SuperBreakes()), SpeedLimitPtr(new ConstSpeedLimit(315))));
}

CarPtr createMcLaren()
{
    return CarPtr(new Car(EnginePtr(new LogarithmicEngine(366)), BreakesPtr(new LogarithmicBreakes()), SpeedLimitPtr(new CxSpeedLimit(0.5, 366))));
}
