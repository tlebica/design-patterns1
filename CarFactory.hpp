#pragma once
#include "Car.hpp"

CarPtr createPorsche();
CarPtr createFerrari();
CarPtr createLamborghini();
CarPtr createMcLaren();
