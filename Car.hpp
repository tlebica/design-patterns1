#pragma once
#include <memory>
#include <Engine.hpp>
#include <SpeedLimit.hpp>
#include <Breakes.hpp>
#include <stack>

class Car
{
public:
    Car(EnginePtr engine, BreakesPtr breakes, SpeedLimitPtr speedLimit)
        :engine(std::move(engine)),
         breakes(std::move(breakes)),
         speedLimit(std::move(speedLimit))
    {}
    int getCurrentSpeedInKmph();
    void accelerate();
    void stop();
    void enter();
    void leave();
    int getNumberOfPassengers();
    virtual ~Car(){}
private:

    struct MovementState
    {
        virtual void enter(Car&) = 0;
        virtual void leave(Car&) = 0;
        ~MovementState(){}
    };

    struct RunningState : MovementState
    {
        void enter(Car &) override{}
        void leave(Car &) override{}
    };

    struct StoppedState : MovementState
    {
        void enter(Car & car) override { car.forwardEnterToOccupationState(); }
        void leave(Car & car) override { car.forwardLeaveToOccupationState(); }
    };

    void forwardEnterToOccupationState();
    void forwardLeaveToOccupationState();

    struct OccupationState
    {
        virtual void accelerate(Car&) = 0;
        virtual void stop(Car&) = 0;
        virtual void leave(Car&) = 0;
        void enter(Car& car) { car.pushOccupationState(); }
        ~OccupationState(){}
    };

    struct EmptyState : OccupationState
    {
        void accelerate(Car&) override {}
        void stop(Car&) override {}
        void leave(Car&) override {}
    };

    struct OccupiedState : OccupationState
    {
        void accelerate(Car& car) override { car.handleAccelerate(); }
        void stop(Car& car) override { car.handleStop(); }
        void leave(Car& car) override { car.popOccupationState(); }
    };

    void pushOccupationState();
    void popOccupationState();
    void handleAccelerate();
    void handleStop();

    int currentSpeed = 0;
    EnginePtr engine;
    BreakesPtr breakes;
    SpeedLimitPtr speedLimit;

    StoppedState stoppedState;
    RunningState runningState;
    MovementState* movementState = &stoppedState;
    std::unique_ptr<OccupationState> occupationState = std::unique_ptr<EmptyState>(new EmptyState());
    std::stack<std::unique_ptr<OccupationState>> occupationStateStack;
};

using CarPtr = std::unique_ptr<Car>;
