#include "TestTrack.hpp"
#include "Car.hpp"
#include <iostream>

bool TestTrack::testCar(Car & car)
{
    return !testAcceleration(car) &&
            testEnter(car) &&
            testAcceleration(car) &&
           !testEnter(car) &&
           !testLeave(car) &&
            testBreakes(car) &&
            testLeave(car) &&
           !testAcceleration(car) &&
            testEnter(car) &&
            testEnter(car) &&
            testEnter(car) &&
            testLeave(car) &&
            testLeave(car) &&
            testAcceleration(car) &&
            testBreakes(car) &&
            testLeave(car) &&
           !testAcceleration(car);
}

bool TestTrack::testAcceleration(Car & car)
{
    car.accelerate();
    car.accelerate();
    car.accelerate();
    car.accelerate();

    return car.getCurrentSpeedInKmph() >= 200;
}

bool TestTrack::testBreakes(Car & car)
{
    car.stop();
    car.stop();
    car.stop();
    car.stop();
    car.stop();

    return car.getCurrentSpeedInKmph() == 0;
}

bool TestTrack::testEnter(Car & car)
{
    int number = car.getNumberOfPassengers();
    car.enter();
    return number + 1 == car.getNumberOfPassengers();
}

bool TestTrack::testLeave(Car & car)
{
    int number = car.getNumberOfPassengers();
    car.leave();
    return number - 1 == car.getNumberOfPassengers();
}



