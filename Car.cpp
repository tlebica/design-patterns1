#include "Car.hpp"

int Car::getCurrentSpeedInKmph()
{
    return currentSpeed;
}


void Car::accelerate()
{
    occupationState->accelerate(*this);
}


void Car::stop()
{
    occupationState->stop(*this);
}

void Car::enter()
{
    movementState->enter(*this);
}

void Car::leave()
{
    movementState->leave(*this);
}

int Car::getNumberOfPassengers()
{
    return occupationStateStack.size();
}

void Car::forwardEnterToOccupationState()
{
    occupationState->enter(*this);
}

void Car::forwardLeaveToOccupationState()
{
    occupationState->leave(*this);
}

void Car::pushOccupationState()
{
    occupationStateStack.push(std::move(occupationState));
    occupationState.reset(new OccupiedState());
}

void Car::popOccupationState()
{
    occupationState.reset(occupationStateStack.top().release());
    occupationStateStack.pop();
}

void Car::handleAccelerate()
{
    currentSpeed += engine->accelerate();
    currentSpeed = speedLimit->limitSpeed(currentSpeed);
    movementState = &runningState;
}

void Car::handleStop()
{
    currentSpeed = breakes->stop(currentSpeed);
    if (currentSpeed == 0)
    {
        engine->reset();
        movementState = &stoppedState;
    }
}
